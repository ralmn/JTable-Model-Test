import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

/**
 * Created by ralmn on 15/06/15.
 */
public abstract class AbstractArrayListTableModel<T> extends AbstractTableModel {

    private ArrayList<T> list;
    private String[] columnsNames;

    public AbstractArrayListTableModel(String[] columnsNames) {
        this.columnsNames = columnsNames;
        this.list = new ArrayList<T>();
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public String getColumnName(int column) {
        return columnsNames[column];
    }

    @Override
    public int getColumnCount() {
        return columnsNames.length;
    }

    @Override
    public abstract Object getValueAt(int row, int column);

    public void addRow(T newRow){
        list.add(newRow);
    }

    /**
     * Suppression d'une ligne du model
     * @param row index de la ligne a supprimer
     */
    public void deleteRow(int row){
        list.remove(row);
    }


    public boolean isCellEditable(int row, int col){
        return false; //Par default on ne voudra pas d'édition
    }

    public ArrayList<T> getData() {
        return list;
    }

}
