/**
 * Created by ralmn on 15/06/15.
 */
public class PersonModel extends AbstractArrayListTableModel<Person> {

    public PersonModel() {
        super(new String[]{"Prenom", "Nom", "Age", "Sexe"});
    }

    @Override
    public Object getValueAt(int row, int column) {
        switch (column){
            case 0:
                return getData().get(row).getPrenom();
            case 1:
                return getData().get(row).getNom();
            case 2:
                return getData().get(row).getAge();
            case 3:
                return getData().get(row).getSexe();
        }
        return null;
    }




}
