import javax.swing.*;

/**
 * Created by ralmn on 15/06/15.
 */
public class App extends JFrame {

    public App(){
        super("app");

        JPanel panel = new JPanel();

        PersonModel model = new PersonModel();
        JTable table = new JTable();
        table.setModel(model);
        model.addRow(new Person("John", "Doe", 42, 'M'));
        model.addRow(new Person("Clara", "Doe", 22, 'F'));
        panel.add(new JScrollPane(table));

        add(panel);

        pack();
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public static void main(String[] args){
        new App();
    }
}
